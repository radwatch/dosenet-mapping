from flask import Flask, request, jsonify, send_file
import sys, time

app = Flask(__name__)

if '--test' in sys.argv:
	CERT_PATH   = 'cert.pem'
	KEY_PATH    = 'key.pem'
	COORDS_PATH = 'tests/coords'
else:
	CERT_PATH   = '/home/pi/dosenet-mapping/mobile/cert.pem'
	KEY_PATH    = '/home/pi/dosenet-mapping/mobile/key.pem'
	COORDS_PATH = '/home/pi/dosenet-mapping/mobile/coords'

@app.route('/')
def index():
	return send_file('index.html')

@app.route('/location', methods=['POST'])
def get_location():
	data = request.get_json()

	with open(COORDS_PATH, 'w') as f:
		f.write(f'{data["latitude"]}, {data["longitude"]}, {int(time.time())}')

	return jsonify({'status': 'success'}), 200

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000, ssl_context=(CERT_PATH, KEY_PATH), debug=True)
