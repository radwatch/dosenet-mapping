import os
import socket
import discord
from datetime import datetime

directory = "/home/pi/data"

with open('/etc/webhook', 'r') as webhook:
    webhook_url = webhook.read().strip()

today = datetime.now().date()
hostname = socket.gethostname()

for filename in os.listdir(directory):
    file_path = os.path.join(directory, filename)
    date = datetime.fromtimestamp(os.path.getmtime(file_path)).date()
    if date == today and ".csv" in filename:
        try:
            with open(file_path, 'rb') as f:
                webhook = discord.SyncWebhook.from_url(webhook_url)
                webhook.send(file=discord.File(f, filename=filename), 
                                content = f"Received file `{filename}` from hostname `{hostname}`.", 
                                username = "RadWatch Data")
            print(f"Sent {filename} to Discord")
        except Exception as e:
            print(f"Error sending {filename}: {str(e)}")